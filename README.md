# My Guide HTML, CSS and Jquery 🙋

Mi guía de cursos de HTML5, CSS y JQuery

## Comenzando 💪🚀

Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.

### Instalación 🔧

Paso a paso de lo que debes ejecutar para tener el proyecto ejecutandose

Primero que nada, clic en [Fork](https://gitlab.com/susananzth/myguide-html-css/-/forks/new)

Inicia el git dentro de tu servidor:
```
git init
```

Luego, clona el repositorio dentro de la carpeta de tu servidor con el siguiente comando:
```
git clone git@gitlab.com:susananzth/myguide-html-css.git
```
_o_
```
git clone https://gitlab.com/susananzth/myguide-html-css.git
```

Listo 😁

## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gitlab.com/susananzth/myguide-html-css/-/blob/master/CONTRIBUTING.md) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Construido con 🛠️

Las herramientas y que utilice para crear este proyecto

* [PildoasInformáticas](https://www.youtube.com/playlist?list=PLU8oAlHdN5BnX63lyAeV0LzLnpGudgRrK) - Primer curso de HTML5
* [Freecodecamp](https://www.freecodecamp.org) - Cursos FrontEnd

## Autores ✒️

* **Susana Piñero** - *FrontEnd + Documentación* - GitLab: [susananzth](https://gitlab.com/susananzth) GitHub: [susananzth](https://github.com/susananzth)

## Licencia 📄

Este proyecto está bajo la Licencia (MIT) - mira el archivo [LICENSE.md](https://gitlab.com/susananzth/myguide-html-css/-/blob/master/LICENSE) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Regalanos una estrella ⭐
* Copia el proyecto en tu cuenta [Fork](https://gitlab.com/susananzth/myguide-html-css/-/forks/new)
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo.
